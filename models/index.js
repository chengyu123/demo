'use strict';
const fs = require('fs');
const path = require('path');
const config = require('../config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(
  config.mysql.sqlBase,
  config.mysql.sqlUser,
  config.mysql.sqlPass,
  {
    dialect: 'mysql',
    host: config.mysql.sqlHost,
    port: config.mysql.sqlPort,
    define: {
      underscored: true
    },
    pool: {
      max: 5,
      min: 1,
      idle: 30000
    },
    timezone: '+08:00',
    logging: console.log
  }
);
let db = {};

fs.readdirSync(__dirname)
  .filter(function(file) {
    return file !== 'index.js';
  })
  .forEach(function(file) {
    let url = path.join(__dirname, file);
    if (fs.statSync(url).isDirectory()) {
      fs.readdirSync(url).forEach(function(subfile) {
        let model = sequelize.import(path.join(__dirname, file, subfile));
        db[model.name] = model;
      });
    } else {
      let model = sequelize.import(url);
      db[model.name] = model;
    }
  });
Object.keys(db).forEach(function(modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
