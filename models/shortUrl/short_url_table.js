/**
 * url与短链接映射关系表
 */

 module.exports = function(sequelize, DataTypes) {
  return sequelize.define('short_url_table', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      // 原始URL
      url: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      // 原始URL对应的短链接
      short_url: {
        type:DataTypes.STRING(50),
        allowNull: false,
      },
      suffix: {
        type:DataTypes.STRING(20),
        allowNull: false,
      }
      },{
      tableName: 'short_url_table',
      timestamps:false,
      updatedAt: false,
      createdAt: false
  });
};
