const express = require('express');
const router = express.Router();
const shortLinkService = require('../service/shortLinkService')

/**
 * 生成短链接，保存原始url和短链接并返回短链接
 * @param {用户输入的原始url} url 
 * @returns shortUrl: 生成的短链接
 */
router.post('/', async function(req, res) {
  const result = await shortLinkService.urlToShortLink(req,res);
  res.send(result);
});

/**
 * 根据用户访问的短链接重定向至原始url
 * @param {短链接} shortUrl 
 * @returns 重定向至原始url
 */
router.get('/:shortUrl', async function(req, res) {
  const result = await shortLinkService.getUrl(req,res);
  res.writeHead(302, {'Location': result});
  res.end()
});

module.exports = router;
