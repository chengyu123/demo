const models = require('../models')
const randomstring = require('randomstring')


async function urlToShortLink(req, res) {
  const { url } = req.body
  if(!url) return {
    status: 400,
    msg: '缺少关键参数',
    data: {}
  }
  let suffix = await generateShortUrl()
  const str = 'http://127.0.0.1:3000/short/link/'+suffix
  await models.short_url_table.create({
    url,
    short_url: str,
    suffix
  })
  const result = str
  return result
}

async function getUrl(req, res) {
  const { shortUrl } = req.params
  const data = await models.short_url_table.findOne({
    attributes: ['url'],
    where: {
      suffix: shortUrl
    }
  })
  return data.url
} 

/**
 * 生成8位随机字符串为短链接,并保证其值唯一
 * @returns str 短链接字符串
 */
async function generateShortUrl() {
  const str = randomstring.generate(8)
  const shortUrlCount = await models.sequelize.query(`SELECT COUNT(*) FROM short_url_table WHERE short_url = '${str}'`,{
    type: models.sequelize.QueryTypes.SELECT
  })
  if(shortUrlCount.count > 0) {
    generateShortUrl()
  }else {
    return str
  }
}

module.exports = {
  urlToShortLink,
  getUrl
}